1 Chánh kiến: Hiểu đúng
- Đọc và kiểm tra chỉ là khâu chuẩn bị
ở mức độ xâu hơn, kinh nghiệm trực tiếp dẫn đến hiểu biết 
đúng đắng, hiểu biết thực tế ít giá trị nếu ko đặt nó vào 
trải nghiệm cá nhân trong cuộc sống.
- KO PHẢI HIỂU BIẾT DỰA TRÊN LÝ THUYẾT, HIỂU NHẬN RA SỰ THẬT
THÔNG QUA TRẢI NGHIỆM CỦA CÁ NHÂN

2. Chánh tư duy: Ý định đúng.
- Ý định bắt nguồn từ trái tim, bao gồm công nhận 
sự bình đẳng với mọi vấn đề của cuộc sống và từ bi 
đối với mọi thứ bắt đầu từ chính bản thân.
- Sự kiên trì vào niềm đam mê cho cuộc hành trình